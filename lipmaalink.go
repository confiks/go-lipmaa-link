package lipmaalink

// Ported from https://github.com/pietgeursen/lipmaa-link

import (
	"math"
)

func Calculate(n uint64) uint64 {
	// Could be more elegant, but this works as well
	if n == math.MaxUint64 {
		return math.MaxUint64 - 4
	}

	var m uint64 = 1
	var po3 uint64 = 3
	u := n

	// Find k such that (3^k - 1)/2 >= n
	for m < n {
		po3 *= 3
		m = (po3 - 1) / 2
	}

	// Find longest possible backjump
	po3 /= 3
	if m != n {
		for u != 0 {
			m = (po3 - 1) / 2
			po3 /= 3
			u %= m
		}

		if m != po3 {
			po3 = m
		}
	}

	return n - po3
}

func Path(n uint64) []uint64 {
	res := []uint64{n}
	for n > 1 {
		n = Calculate(n)
		res = append(res, n)
	}

	return res
}

func PathLength(n uint64) uint64 {
	path := Path(n)
	return uint64(len(path))
}
