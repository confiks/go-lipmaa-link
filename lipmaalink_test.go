package lipmaalink

import (
	"math"
	"reflect"
	"testing"
)

type testcase struct {
	input  uint64
	output uint64
}

func TestCalculate(t *testing.T) {
	tcs := []testcase{
		{1, 0},
		{2, 1},
		{3, 2},
		{4, 1},
		{5, 4},
		{6, 5},
		{7, 6},
		{8, 4},
		{9, 8},
		{10, 9},
		{11, 10},
		{12, 8},
		{13, 4},
		{14, 13},
		{15, 14},
		{16, 15},
		{17, 13},
		{18, 17},
		{19, 18},
		{20, 19},
		{21, 17},
		{22, 21},
		{23, 22},
		{24, 23},
		{25, 21},
		{26, 13},
		{27, 26},
		{28, 27},
		{29, 28},
		{30, 26},
		{31, 30},
		{32, 31},
		{33, 32},
		{34, 30},
		{35, 34},
		{36, 35},
		{37, 36},
		{38, 34},
		{39, 26},
		{40, 13},
		{121, 40},
		{2391484, 797161},
		{14348906, 7174453},
		{1162261466, 581130733},
		{31526437013463, 31526437013450},
		{math.MaxUint32, 4294967294},
		{math.MaxUint64, 18446744073709551611},
	}

	for i, tc := range tcs {
		output := Calculate(tc.input)
		if output != tc.output {
			t.Fatalf(
				"Calculate testcase #%d with input %d gave unexpected output %d instead of expected %d",
				i, tc.input, output, tc.output,
			)
		}
	}
}

func TestPathLength(t *testing.T) {
	tcs := []testcase{
		{1, 1},
		{2, 2},
		{3, 3},
		{4, 2},
		{1162261466, 20},
	}

	for i, tc := range tcs {
		output := PathLength(tc.input)
		if output != tc.output {
			t.Fatalf(
				"PathLength testcase #%d with input %d gave unexpected output %d instead of expected %d",
				i, tc.input, output, tc.output,
			)
		}
	}
}

func TestPath(t *testing.T) {
	res := []uint64{1162261466, 581130733, 193710244, 64570081, 21523360, 7174453, 2391484, 797161, 265720, 88573, 29524, 9841, 3280, 1093, 364, 121, 40, 13, 4, 1}
	if !reflect.DeepEqual(Path(1162261466), res) {
		t.Fatal("TestPath failed")
	}
}
